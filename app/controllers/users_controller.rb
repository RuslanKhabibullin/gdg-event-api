class UsersController < ApplicationController
  def find
    user = User.find_by(email: params[:email])
    if user
      user.increment!(:sign_in_count)
      head :ok
    else
      head :unauthorized
    end
  end

  def index
    users = User.signed_in
    render json: users, meta: { users_length: users.length }
  end
end
