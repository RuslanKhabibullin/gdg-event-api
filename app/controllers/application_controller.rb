class ApplicationController < ActionController::API
  before_action { headers['Access-Control-Allow-Origin'] = '*' }
end
