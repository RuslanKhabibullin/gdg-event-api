class User < ApplicationRecord
  validates :email, presence: true,
            uniqueness: { case_sensitive: false }

  scope :signed_in, -> { where("sign_in_count > ?", 0) }
end
