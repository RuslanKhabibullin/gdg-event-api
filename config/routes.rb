Rails.application.routes.draw do
  get 'find', to: 'users#find'
  resources :users, only: %i(index)
end
