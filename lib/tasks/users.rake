namespace :users do
  desc 'Import users'
  task import: :environment do
    xlsx = Roo::Spreadsheet.open('db/seeds/users.xlsx')
    xlsx.each_row_streaming(offset: 1) do |row|
      if email_cell = row[2]
        email =  email_cell.cell_value
        begin
          User.create!(email: email)
        rescue ActiveRecord::RecordInvalid => e
          p "#{ email} - #{ e.message }"
          next
        end
      end
    end
  end
end
